const flaxaudio = require("../"); // For local development

module.exports = (eleventyConfig) => {
  eleventyConfig.addPlugin(flaxaudio, {
    path: `/audio/`,
    audioEl: false
  });
  eleventyConfig.addPassthroughCopy("audio");
};
